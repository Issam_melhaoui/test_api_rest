REST API Testing:

- Using one of the APIs available at https://openweathermap.org/api
- Create a Postman collection that is checking the weather in New York
- Add a test on the max_temperature: if max_temperature > 10degree Celsius, the
test should fail
